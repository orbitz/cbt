open Async.Std

module Entry : sig
  type resolved =
    | Unresolved
    | Success
    | Failure

  type t = { owner           : string
           ; id              : string
           ; title           : string
           ; validation_date : Core.Time.t
           ; comments        : string list
           ; public          : bool
           ; tags            : string list
           ; resolved        : resolved
           ; version         : string
           }
end

type t

val create : unit -> t

val get_by_id : t -> string -> (Entry.t, [> `Not_found of string ]) Deferred.Result.t

val get_by_owner : t -> string -> Entry.t list Deferred.t

val put : t -> Entry.t -> (string, [> `Version_conflict of Entry.t ]) Deferred.Result.t


