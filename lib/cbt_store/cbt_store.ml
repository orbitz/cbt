open Core.Std
open Async.Std

module Entry = struct
  type resolved =
    | Unresolved
    | Success
    | Failure

  type t = { owner           : string
           ; id              : string
           ; title           : string
           ; validation_date : Core.Time.t
           ; comments        : string list
           ; public          : bool
           ; tags            : string list
           ; resolved        : resolved
           ; version         : string
           }
end

type t = Entry.t String.Table.t

let id_counter = ref 0

let create () =
  String.Table.create ()

let get_by_id t id =
  match Hashtbl.find t id with
    | Some entry ->
      Deferred.return (Ok entry)
    | None ->
      Deferred.return (Error (`Not_found id))

let get_by_owner t owner =
  Hashtbl.to_alist t
    |> List.map ~f:snd
    |> List.filter ~f:(fun e -> e.Entry.owner = owner)
    |> Deferred.return

let put_new_entry t entry =
  let id = Int.to_string !id_counter in
  id_counter := !id_counter + 1;
  let entry = { entry with Entry.id = id } in
  Hashtbl.set t ~key:entry.Entry.id ~data:entry;
  Deferred.return (Ok id)

let replace_entry t entry =
  match Hashtbl.find t entry.Entry.id with
    | Some e when entry.Entry.version <> e.Entry.version ->
      Deferred.return (Error (`Version_conflict e))
    | Some _
    | None -> begin
      Hashtbl.set t ~key:entry.Entry.id ~data:entry;
      Deferred.return (Ok entry.Entry.id)
    end

let put t = function
  | entry when entry.Entry.id = "" ->
    put_new_entry t entry
  | entry ->
    replace_entry t entry
