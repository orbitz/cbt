open Core.Std

module Location : sig
  type t = { source   : string
	   ; line     : int
	   ; column   : int
	   ; position : int
	   }
end

module Key : sig
  type t = string list
end

type t

val of_string : string -> (t, [> `Parse_error of (string * Location.t) ]) Result.t

val get_bool : Key.t -> t -> bool
val get_int : Key.t -> t -> int
val get_float : Key.t -> t -> float
val get_string : Key.t -> t -> string
val get_table : Key.t -> t -> t
val get_bool_array : Key.t -> t -> bool list
val get_int_array : Key.t -> t -> int list
val get_float_array : Key.t -> t -> float list
val get_string_array : Key.t -> t -> string list
val get_config_array : Key.t -> t -> t list
