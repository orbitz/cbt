open Core.Std

module Location = struct
  type t = { source   : string
	   ; line     : int
	   ; column   : int
	   ; position : int
	   }
end


module Key = struct
  type t = string list
end

type t = Toml.Value.table

let of_string str =
  try
    Ok (Toml.Parser.from_string str)
  with
    | Toml.Parser.Error (err_str, loc) ->
      let module P = Toml.Parser in
      let module L = Location in
      let nloc = L.({ source   = loc.P.source
		    ; line     = loc.P.line
		    ; column   = loc.P.column
		    ; position = loc.P.position
		    })
      in
      Error (`Parse_error (err_str, nloc))

let rec apply_key key t f =
  let module TTK = Toml.Table.Key in
  match key with
    | []    -> failwith "nyi"
    | [k]   -> f (TTK.of_string k) t
    | k::ks -> apply_key ks (Toml.get_table (TTK.of_string k) t) f

let get_bool k t =
  apply_key k t Toml.get_bool

let get_int k t =
  apply_key k t Toml.get_int

let get_float k t =
  apply_key k t Toml.get_float

let get_string k t =
  apply_key k t Toml.get_string

let get_table k t =
  apply_key k t Toml.get_table

let get_bool_array k t =
  apply_key k t Toml.get_bool_array

let get_int_array k t =
  apply_key k t Toml.get_int_array

let get_float_array k t =
  apply_key k t Toml.get_float_array

let get_string_array k t =
  apply_key k t Toml.get_string_array

let get_config_array k t =
  apply_key k t Toml.get_table_array
