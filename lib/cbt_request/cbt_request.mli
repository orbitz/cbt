module Insert : sig
  type t = { title           : string
           ; validation_date : Core.Time.t
           ; comments        : string list
           ; public          : bool
           ; tags            : string list
           }
end

module Update : sig
  type t = { id              : string
           ; title           : string option
           ; validation_date : Core.Time.t option
           ; comments        : string list option
           ; public          : bool option
           ; tags            : string list
           ; version         : string
           }
end

module Delete : sig
  type t = { id      : string
           ; version : string
           }
end

type t =
  | Insert of Insert.t
  | Update of Update.t
  | Delete of Delete.t
