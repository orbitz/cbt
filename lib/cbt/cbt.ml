open Core.Std
open Async.Std

let main () =
  Random.self_init ();
  Exn.handle_uncaught ~exit:true (fun () ->
    Command.run ~version:"1.0.0" ~build_info:"N/A"
      (Command.group ~summary:"cbt commands"
         [ ("server", Cbt_cmd_server.command)
         ]))

let () =
  main ()

