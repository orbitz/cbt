open Async.Std

val start :
  Mconfig.t list ->
  (Zolog_event.Id.t Zolog.t, [> `Bad_loggers of string list ]) Deferred.Result.t
