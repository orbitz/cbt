module StdHashtbl = Hashtbl

open Core.Std
open Async.Std

module Server   = Cohttp_async.Server
module Body     = Cohttp_async.Body
module Request  = Cohttp_async.Request
module Response = Cohttp_async.Response
module S        = Cbt_state
module Logger   = Zolog_event.Make(Zolog_event.Id)

let upsert_of_json = Cbt_upsert_of_json.convert

let json_of_entry = Cbt_json_of_entry.convert

let entry_of_insert owner insert =
  let module I = Cbt_request.Insert in
  Cbt_store.Entry.({ owner           = owner
                   ; id              = ""
                   ; title           = insert.I.title
                   ; validation_date = insert.I.validation_date
                   ; comments        = insert.I.comments
                   ; public          = insert.I.public
                   ; tags            = insert.I.tags
                   ; resolved        = Unresolved
                   ; version         = ""
                   })

let string_of_entry entry =
  Json_io.string_of_json (json_of_entry entry)

let string_of_entries entries =
  Json_io.string_of_json
    (Json_type.Build.objekt
       [ ("entries", Json_type.Build.list json_of_entry entries) ])

let exec_insert state authorization insert =
  let entry = entry_of_insert authorization insert in
  Cbt_store.put state.Cbt_state.store entry
  >>= function
    | Ok id -> begin
      Logger.info
        state.Cbt_state.log
        ~n:["cbt"; "server"; "http"; "insert"]
        (sprintf "Created id: %s" id)
      >>= fun _ ->
      Deferred.return (Ok id)
    end
    | Error (`Version_conflict _) ->
      Deferred.return (Error `Version_conflict)

let exec_update state update =
  failwith "nyi"

let exec_upsert state authorization body =
  let json = Json_io.json_of_string body in
  match upsert_of_json json with
    | Ok (`Insert insert) ->
      exec_insert state authorization insert
    | Ok (`Update update) ->
      exec_update state update
    | Error () ->
      Deferred.return (Error (`Unknown_request body))

let exec_search_by_id state authorization id =
  Logger.counter
    state.Cbt_state.log
    ~n:["cbt"; "server"; "http"; "search"; "id"]
    1
  >>= fun _ ->
  Cbt_store.get_by_id state.Cbt_state.store id
  >>= function
    | Ok entry when entry.Cbt_store.Entry.owner = authorization ->
      Deferred.return (Ok (string_of_entry entry))
    | Ok _
    | Error (`Not_found _)->
      Deferred.return (Error `Bad_search)

let exec_search_by_owner state owner =
  Logger.counter
    state.Cbt_state.log
    ~n:["cbt"; "server"; "http"; "search"; "owner"]
    1
  >>= fun _ ->
  Cbt_store.get_by_owner state.Cbt_state.store owner
  >>= fun entries ->
  Deferred.return (Ok (string_of_entries entries))

let exec_search state authorization uri =
  match Uri.get_query_param uri "id" with
    | Some id ->
      exec_search_by_id state authorization id
    | None ->
      exec_search_by_owner state authorization

let dispatch_request state authorization body uri = function
  | "/upsert" ->
    exec_upsert state authorization body
  | "/search" ->
    exec_search state authorization uri
  | path ->
    Deferred.return (Error (`Unknown_request path))

let error_response = function
  | `Version_conflict ->
    (Response.make ~status:`Bad_request (), Body.of_string "version_conflict")
  | `Unknown_request req ->
    (Response.make ~status:`Bad_request (), Body.of_string req)
  | `Bad_search ->
    (Response.make ~status:`Bad_request (), Body.of_string "bad_search")

let process_http state body_conduit req =
  let headers = Request.headers req in
  match Cohttp.Header.get headers "authorization" with
    | Some authorization -> begin
      Body.to_string body_conduit
      >>= fun body ->
      dispatch_request
        state
        authorization
        body
        (Request.uri req)
        (Uri.path (Request.uri req))
      >>= function
        | Ok resp ->
          Deferred.return
            (Response.make ~status:`OK (), Body.of_string resp)
        | Error err ->
          Deferred.return (error_response err)
    end
    | None -> begin
      Logger.counter
        state.Cbt_state.log
        ~n:["cbt"; "server"; "http"; "bad_auth"]
        1
      >>= fun _ ->
      Deferred.return
        (Response.make ~status:`Unauthorized (), Body.of_string "")
    end

let start_server state _config server_config =
  match Mconfig.get_string ["args"; "type"] server_config with
    | "http" ->
      let port = Mconfig.get_int ["args"; "port"] server_config in
      Server.create
        ~on_handler_error:`Raise
        (Tcp.on_port port)
        (fun ~body _ req -> process_http state body req)
    | _ ->
      failwith "nyi"

let start config =
  let loggers = Mconfig.get_config_array ["log"] config in
  let store = Cbt_store.create () in
  Cbt_logger.start loggers
  >>= function
    | Ok log ->
      let state = Cbt_state.({ log; store }) in
      let acceptors = Mconfig.get_config_array ["acceptors"] config in
      let servers =
        List.map
          ~f:(start_server state config)
          acceptors
      in
      Deferred.all servers
      >>= fun _ ->
      Deferred.never ()
    | Error (`Bad_loggers errs) -> begin
      eprintf
        "Unable to start loggers: %s\n%!"
        (String.concat ~sep:", " errs);
      failwith "Failed to start"
    end

