type upsert = [ `Insert of Cbt_request.Insert.t
              | `Update of Cbt_request.Update.t
              ]

val convert : Json_type.t -> (upsert, unit) Core.Result.t
