open Core.Std
open Async.Std

module Console_backend = Zolog_event_console_backend.Make(Zolog_event.Id)
module Writer_backend  = Zolog_event_writer_backend.Make(Zolog_event.Id)

let start loggers =
  Zolog.start ()
  >>= fun logger ->
  Deferred.List.map
    ~f:(fun logger_config ->
      match Mconfig.get_string ["type"] logger_config with
        | "console" ->
          Console_backend.create
            Writer_backend.default_with_metrics_formatter
          >>= fun backend ->
          Zolog.add_handler logger (Console_backend.handler backend)
          >>= fun _ ->
          Deferred.return None
        | bad_logger ->
          Deferred.return (Some bad_logger))
    loggers
  >>= fun logger_res ->
  match Option.all (List.filter ~f:Option.is_some logger_res) with
    | Some []
    | None      -> Deferred.return (Ok logger)
    | Some errs -> Deferred.return (Error (`Bad_loggers errs))
