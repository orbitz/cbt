module StdHashtbl = Hashtbl

open Core.Std

module Build = Json_type.Build

let string_of_resolved = function
  | Cbt_store.Entry.Unresolved -> "Unresolved"
  | Cbt_store.Entry.Success    -> "Success"
  | Cbt_store.Entry.Failure    -> "Failure"

let convert entry =
  let module E = Cbt_store.Entry in
  Build.objekt
    [ ("id", Build.string entry.E.id)
    ; ("title", Build.string entry.E.title)
    ; ("validation_date", Build.string (Time.to_string entry.E.validation_date))
    ; ("comments", Build.list Build.string entry.E.comments)
    ; ("public", Build.bool entry.E.public)
    ; ("tags", Build.list Build.string entry.E.tags)
    ; ("resolved", Build.string (string_of_resolved entry.E.resolved))
    ; ("version", Build.string entry.E.version)
    ]
