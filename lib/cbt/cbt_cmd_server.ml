open Core.Std
open Async.Std

module Flag = struct
  open Command.Spec

  let config_file =
    flag "-config" ~doc:" Config file" (required string)
end

let cmd_event_engine_start =
  Command.async_basic
    ~summary:"Start a server"
    Command.Spec.(empty +> Flag.config_file)
    (fun config_file () ->
      Reader.file_contents config_file
      >>= fun content ->
      match Mconfig.of_string content with
	| Ok mconfig ->
	  Cbt_server.start mconfig
	| Error _ ->
	  failwith "config file failed")

let command =
  Command.group
    ~summary:"Event engine commands"
    [ ("start",  cmd_event_engine_start)
    ]
