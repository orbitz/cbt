module StdHashtbl = Hashtbl

open Core.Std

module Browse = Json_type.Browse

type upsert = [ `Insert of Cbt_request.Insert.t
              | `Update of Cbt_request.Update.t
              ]


module Json_monad = struct
  let ( >>= ) = Option.( >>= )

  let safe f v =
    Option.try_with (fun () -> f v)

  let field table typ name =
    Browse.optfield table name >>= safe typ

  let list = Browse.list

  let string = Browse.string

  let int = Browse.int

  let bool = Browse.bool
end

let parse_update table =
  let open Json_monad in
  field table string "id"
  >>= fun id ->
  let title = field table string "title" in
  let validation_date =
    field table (Fn.compose Time.of_string string) "validation_date"
  in
  let comments = field table (list string) "comments" in
  let public = field table bool "public" in
  field table (list string) "tags"
  >>= fun tags ->
  field table string "version"
  >>= fun version ->
  Some Cbt_request.Update.({ id
                           ; title
                           ; validation_date
                           ; comments
                           ; public
                           ; tags
                           ; version
                           })

let parse_insert table =
  let open Json_monad in
  field table string "title"
  >>= fun title ->
  field table (Fn.compose Time.of_string string) "validation_date"
  >>= fun validation_date ->
  field table (list string) "comments"
  >>= fun comments ->
  field table bool "public"
  >>= fun public ->
  field table (list string) "tags"
  >>= fun tags ->
  Some Cbt_request.Insert.({ title
                           ; validation_date
                           ; comments
                           ; public
                           ; tags
                           })


let convert json =
  try
    let objekt = Browse.objekt json in
    let table  = Browse.make_table objekt in
    match parse_update table with
      | None -> begin
        match parse_insert table with
          | Some insert ->
            Ok (`Insert insert)
          | None ->
            Error ()
      end
      | Some update ->
        Ok (`Update update)
  with
    | _ ->
      Error ()
